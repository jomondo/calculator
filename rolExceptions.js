class NoRolNameError extends Error {
  constructor() {
    super("No existeix eixe rol");
    this.name = 'NoRolName';
  }
}

module.exports = NoRolNameError;