const User = require('./classUser');

class Person extends User{
  constructor(name,rol,surname) {
  	super(name,rol);
    this.name = name;
    this.surname = surname;
    this.rol = rol;
    
  }

}

module.exports = Person;