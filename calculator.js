const NoCalculatorFunctionNameError = require('./customExceptions');
const NoRolNameError = require('./rolExceptions');
const ClassPerson = require('./classPerson');
const NoPermissionError = require('./permisionException');

let fAdd = function(array_) {   
  return array_.reduce((valorAnterior, valorActual) => {
     return valorAnterior + valorActual;
  });
}

let fMultiplier = function(array_) {
  return array_.reduce((valorAnterior, valorActual)=>{
     return valorAnterior * valorActual;
  });
} 
//It doubles every single item of the array
let fDoublefier = function(array_){
  return array_.map(function(valorActual){
    return valorActual*2;
  });
}

let operationObject={
  'add':fAdd,
  'multiplier':fMultiplier,
  'doublefier':fDoublefier
};

let estudiantOperandPermited = ["add","multiplier"];
let professorOperandPermited = ["doublefier"];

let rolPermited={
  'Estudiant':estudiantOperandPermited,
  'Professor':professorOperandPermited
  
};

function calculator(array_,operation_,person_){  
  try {
    if (array_.length<=0) return 0;
    let fOperation=operationObject[operation_];

    if (typeof fOperation !== "function"){ 
      throw new NoCalculatorFunctionNameError();

    }else{
      if(!rolPermited.hasOwnProperty(person_.rol)){
        throw new NoRolNameError();

      }else{
        let operationPermited=rolPermited[person_.rol];
        let g = operationPermited.filter(c => c == operation_);
        //console.log(g);
        if (!g[0]) {
          return new NoPermissionError().name;
        }else{
          return operationObject[operation_](array_);
        }
      } 
    }

  }catch (error) {
    console.log(error.name+" "+error.message);
    return error.constructor;

  }
  
}

let Person = new ClassPerson("Joan","Estudiant","Montes Doria");
console.log(calculator([2,4],"add",Person));

module.exports = calculator;